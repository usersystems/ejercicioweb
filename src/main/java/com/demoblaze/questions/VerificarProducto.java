package com.demoblaze.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static com.demoblaze.userinterfaces.PortalPaginaPrincipal.TXT_NOMBRE_PROD;

public class VerificarProducto implements Question<String> {
    public static Question<String> value() {
        return new VerificarProducto();

    }

    @Override
    public String answeredBy(Actor actor) {
        return TXT_NOMBRE_PROD.resolveFor(actor).getText();
    }


}