package com.demoblaze.questions;

import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.TextContent;

import static com.demoblaze.userinterfaces.PortalPaginaPrincipal.TXT_ORDEN;
import static com.demoblaze.userinterfaces.PortalPaginaPrincipal.TXT_NUMERO_TARJETA;


public class VerificarOrden {
    public static Question<String> nombreOrden() {
        return actor -> TextContent.of(TXT_ORDEN).viewedBy(actor).asString().trim().substring(56, 61);

    }

    public static Question<String> numeroTarjeta() {
        return actor -> TextContent.of(TXT_NUMERO_TARJETA).viewedBy(actor).asString().trim().substring(40, 50);

    }

}
