package com.demoblaze.userinterfaces;


import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://www.demoblaze.com/ ")
public class PortalPaginaPrincipal extends PageObject {

    public static final Target BTN_LAPTOPS = Target.the("Seleccionar opcion Laptop").located(By.xpath("//a[@href='#'][contains(.,'Laptops')]"));
    public static final Target BTN_MACBOOKPRO6 = Target.the("Seleccionar Producto MacBookPro").located(By.xpath("//a[contains(.,'MacBook Pro')]"));
    public static final Target BTN_INGLOGIN = Target.the("Boton ara realizar el login").located(By.id("login2"));
    public static final Target TXT_NOMBREUSER = Target.the("Campo nombre de usuario").located(By.id("loginusername"));
    public static final Target TXT_PASSWORD = Target.the("Campo clave").located(By.id("loginpassword"));
    public static final Target BTN_LOGIN = Target.the("Boton de login").located(By.xpath("//*/button[contains(text(),'Log in')]"));
    public static final Target BTN_AGREGAR_PROD = Target.the("Boton agregar producto").located(org.openqa.selenium.By.xpath("//*/a[@class=\"btn btn-success btn-lg\"]"));
    public static final Target BTN_LISTADO_PROD= Target.the("Boton listado produtos").located(org.openqa.selenium.By.id("cartur"));
    public static final Target TXT_NOMBRE_PROD = Target.the("Campo producto listado compras").located(By.xpath("//*/td[contains(text(),'Mac')]"));
    public static final Target TXT_PEDIR_PROD = Target.the("Boton Place Order").located(By.xpath("//div[@class=\"col-lg-1\"]/button"));

    public static final Target TXT_NOMBRE_ORD = Target.the("Campo nombre").located(By.xpath("//*/input[@id=\"name\"]"));
    public static final Target TXT_PAIS = Target.the("Campo pais en el pedido").located(By.xpath("//*/input[@id=\"country\"]"));
    public static final Target TXT_CIUDAD = Target.the("Campo ciudad en el pedido").located(By.xpath("//*/input[@id=\"city\"]"));
    public static final Target TXT_TARJ_CREDITO = Target.the("Campo tarjeta de credito en el pedido").located(By.xpath("//*/input[@id=\"card\"]"));
    public static final Target TXT_MES = Target.the("Campo  mes en el pedido").located(By.xpath("//*/input[@id=\"month\"]"));
    public static final Target TXT_YEAR = Target.the("Campo year en el pedido").located(By.xpath("//*/input[@id=\"year\"]"));
    public static final Target TXT_CONFIRMAR_COMPRAR = Target.the("Campo Compra").located(By.xpath("//*/button[contains(text(),'Purchase')]"));
    public static final Target TXT_NUMERO_TARJETA = Target.the("Campo datos finales Nro tarjeta Orden").located(By.xpath("//*/div[@class=\"sweet-alert  showSweetAlert visible\"]/p"));
    public static final Target TXT_ORDEN = Target.the("Campo orden").located(By.xpath("//*/div[@class=\"sweet-alert  showSweetAlert visible\"]/p"));


}
