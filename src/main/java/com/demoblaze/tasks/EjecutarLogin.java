package com.demoblaze.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

import static com.demoblaze.userinterfaces.PortalPaginaPrincipal.BTN_INGLOGIN;
import static net.serenitybdd.screenplay.Tasks.instrumented;

public class EjecutarLogin implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(BTN_INGLOGIN));

    }

    public static EjecutarLogin deUsuario() {
        return instrumented(EjecutarLogin.class);
    }

}


