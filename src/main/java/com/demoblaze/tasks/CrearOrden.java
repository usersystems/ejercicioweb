package com.demoblaze.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

import static com.demoblaze.userinterfaces.PortalPaginaPrincipal.TXT_CONFIRMAR_COMPRAR;
import static net.serenitybdd.screenplay.Tasks.instrumented;

public class CrearOrden implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(TXT_CONFIRMAR_COMPRAR));
    }

    public static CrearOrden alComprarProducto() {
        return instrumented(CrearOrden.class);
    }
}
