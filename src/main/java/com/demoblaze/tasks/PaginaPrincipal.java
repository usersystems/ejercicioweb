package com.demoblaze.tasks;

import com.demoblaze.userinterfaces.PortalPaginaPrincipal;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class PaginaPrincipal implements Task {

    private PortalPaginaPrincipal portalPaginaPrincipal;

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(Open.browserOn(portalPaginaPrincipal));


    }

    public static PaginaPrincipal aWeb() {
        return instrumented(PaginaPrincipal.class);


    }


}
