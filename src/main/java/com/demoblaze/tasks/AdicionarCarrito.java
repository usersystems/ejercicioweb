package com.demoblaze.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

import static com.demoblaze.userinterfaces.PortalPaginaPrincipal.BTN_LISTADO_PROD;
import static net.serenitybdd.screenplay.Tasks.instrumented;

public class AdicionarCarrito implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(BTN_LISTADO_PROD));
    }

    public static AdicionarCarrito DemoBlaze() {
        return instrumented(AdicionarCarrito.class);
    }
}

