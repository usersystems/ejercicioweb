package com.demoblaze.tasks;

import com.demoblaze.utils.DatosBasicos;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Enter;

import static com.demoblaze.userinterfaces.PortalPaginaPrincipal.TXT_NOMBREUSER;
import static com.demoblaze.userinterfaces.PortalPaginaPrincipal.TXT_PASSWORD;
import static net.serenitybdd.screenplay.Tasks.instrumented;

public class IngresarCredenciales implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Enter.theValue(DatosBasicos.nombreUsuario).into(TXT_NOMBREUSER),
                Enter.theValue(DatosBasicos.password).into(TXT_PASSWORD));


    }

    public static IngresarCredenciales paraIngresarAPerfil() {
        return instrumented(IngresarCredenciales.class);

    }
}