package com.demoblaze.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static com.demoblaze.userinterfaces.PortalPaginaPrincipal.TXT_PEDIR_PROD;
import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class PedirProducto implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(WaitUntil.the(TXT_PEDIR_PROD,isVisible()).forNoMoreThan(2).seconds(),
                Click.on(TXT_PEDIR_PROD));

    }

    public static PedirProducto deProductos() {
        return instrumented(PedirProducto.class);
    }
}