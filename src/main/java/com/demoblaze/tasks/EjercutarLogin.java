package com.demoblaze.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

import static com.demoblaze.userinterfaces.PortalPaginaPrincipal.BTN_LOGIN;
import static net.serenitybdd.screenplay.Tasks.instrumented;

public class EjercutarLogin implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(BTN_LOGIN));
    }

        public static EjercutarLogin Registrado() {
            return instrumented(EjercutarLogin.class);

    }
}
