package com.demoblaze.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

import static com.demoblaze.userinterfaces.PortalPaginaPrincipal.*;
import static net.serenitybdd.screenplay.Tasks.instrumented;

public class EscogerProducto implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(BTN_LAPTOPS));
        actor.attemptsTo(Click.on(BTN_MACBOOKPRO6));


    }
    public static EscogerProducto Listado() {
        return instrumented(EscogerProducto.class);
    }
}
