package com.demoblaze.tasks;

import com.demoblaze.interactions.DatosBasicos;
import com.demoblaze.models.DatosIngreso;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class DiligenciarFormulario implements Task {

    private DatosIngreso datosIngreso;

    public DiligenciarFormulario(DatosIngreso datosIngreso) {
        this.datosIngreso = datosIngreso;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(DatosBasicos.enLosEspacios(datosIngreso));
    }

    public static DiligenciarFormulario paraOrdenDePago(DatosIngreso datosIngreso) {
        return instrumented(DiligenciarFormulario.class, datosIngreso);
    }
}