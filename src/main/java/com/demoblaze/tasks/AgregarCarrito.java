package com.demoblaze.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static com.demoblaze.userinterfaces.PortalPaginaPrincipal.BTN_AGREGAR_PROD;
import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class AgregarCarrito implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(WaitUntil.the(BTN_AGREGAR_PROD, isVisible()).forNoMoreThan(2).seconds(), Click.on(BTN_AGREGAR_PROD));
    }

    public static AgregarCarrito deLaTienda() {
        return instrumented(AgregarCarrito.class);
    }
}
