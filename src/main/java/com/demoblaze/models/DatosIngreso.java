package com.demoblaze.models;

public class DatosIngreso {

    private String nombre;
    private String nroTarjeta;
    private String pais;
    private String ciudad;
    private String mes;
    private String tiempo;

    public DatosIngreso(String nombre, String nroTarjeta, String pais, String ciudad, String mes, String tiempo) {
        this.nombre = nombre;
        this.nroTarjeta = nroTarjeta;
        this.pais = pais;
        this.ciudad = ciudad;
        this.mes = mes;
        this.tiempo = tiempo;
    }

    public String getNombre() {
        return nombre;
    }

    public String getNroTarjeta() {
        return nroTarjeta;
    }

    public String getPais() {
        return pais;
    }

    public String getCiudad() {
        return ciudad;
    }

    public String getMes() {
        return mes;
    }

    public String getTiempo() {
        return tiempo;
    }
}
