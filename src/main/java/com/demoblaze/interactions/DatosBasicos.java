package com.demoblaze.interactions;

import com.demoblaze.models.DatosIngreso;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.actions.Enter;

import static com.demoblaze.userinterfaces.PortalPaginaPrincipal.*;
import static net.serenitybdd.screenplay.Tasks.instrumented;

public class DatosBasicos implements Interaction {

    private DatosIngreso datosIngreso;


    public DatosBasicos(DatosIngreso datosIngreso) {
        this.datosIngreso = datosIngreso;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Enter.theValue(datosIngreso.getNombre()).into(TXT_NOMBRE_ORD),
                Enter.theValue(datosIngreso.getPais()).into(TXT_PAIS),
                Enter.theValue(datosIngreso.getCiudad()).into(TXT_CIUDAD),
                Enter.theValue(datosIngreso.getNroTarjeta()).into(TXT_TARJ_CREDITO),
                Enter.theValue(datosIngreso.getMes()).into(TXT_MES),
                Enter.theValue(datosIngreso.getTiempo()).into(TXT_YEAR)
        );
    }

    public static DatosBasicos enLosEspacios(DatosIngreso datosIngreso) {
        return instrumented(DatosBasicos.class, datosIngreso);
    }
}
