#Author: xiomara.f@gmail.com
#language: es

@Regresion
Característica: Realizar la compra de uno o varios productos
  Como usuario de Demoblaze
  Quiero realizar la compra de un producto
  A través de la pagina web demoblaze

  Antecedentes:
    Dado que el usuario ingresa a la pagina web de  demoblaze


  @EscenarioExitoso
  Esquema del escenario: Verificar compra producto
    Cuando se agrega el  producto al carro de compras
    Entonces verifica la compra del producto
    Y los datos para la orden de compra son "<nombre>" y "<nroTarjeta>"
      | nombre   | nroTarjeta   | pais   | ciudad   | mes   | tiempo   |
      | <nombre> | <nroTarjeta> | <pais> | <ciudad> | <mes> | <tiempo> |
    Ejemplos:
      | nombre | nroTarjeta | pais     | ciudad | mes | tiempo |
      | pepea  | 1234567890 | Colombia | Bogota | Nov | 2024   |


