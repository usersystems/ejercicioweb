package com.demoblaze.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "src/test/resources/features/compraWeb.feature",
        glue = "com.demoblaze",
        monochrome = true,
        plugin = {"pretty", "html:target/cucumber"},
        snippets = SnippetType.CAMELCASE,
        tags = "@EscenarioExitoso")

public class CompraFinalRunner {

}
