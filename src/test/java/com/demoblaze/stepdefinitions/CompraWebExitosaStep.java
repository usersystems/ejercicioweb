package com.demoblaze.stepdefinitions;

import com.demoblaze.exceptions.MensaExcepciones;
import com.demoblaze.models.DatosIngreso;
import com.demoblaze.questions.VerificarOrden;
import com.demoblaze.questions.VerificarProducto;
import com.demoblaze.tasks.*;
import com.demoblaze.userinterfaces.PortalPaginaPrincipal;
import com.demoblaze.utils.DatosBasicos;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Managed;
import org.openqa.selenium.WebDriver;

import java.util.List;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.core.IsEqual.equalTo;

public class CompraWebExitosaStep {
    @Managed(driver = "chrome")
    private WebDriver hisBrowser;
    private Actor usuariofinal = Actor.named("usuario");

    @Before
    public void configuracionInicial() {
        usuariofinal.can(BrowseTheWeb.with(hisBrowser));
        usuariofinal.wasAbleTo(Open.browserOn(new PortalPaginaPrincipal()));
    }


    @Dado("^que el usuario ingresa a la pagina web de  demoblaze$")
    public void queElUsuarioIngresaALaPaginaWebDeDemoblaze() {
        usuariofinal.attemptsTo(EjecutarLogin.deUsuario());
        usuariofinal.attemptsTo(IngresarCredenciales.paraIngresarAPerfil());
        usuariofinal.attemptsTo(EjercutarLogin.Registrado());
    }


    @Cuando("^se agrega el  producto al carro de compras$")
    public void seAgregaElProductoAlCarroDeCompras()  {
        usuariofinal.attemptsTo(EscogerProducto.Listado());
        usuariofinal.attemptsTo(AgregarCarrito.deLaTienda());
        usuariofinal.attemptsTo(EjercutarAviso.deProductoAgregado(hisBrowser));
        usuariofinal.attemptsTo(AdicionarCarrito.DemoBlaze());

    }

    @Entonces("^verifica la compra del producto$")
    public void verificaLaCompraDelProducto() {
        usuariofinal.should(seeThat("El nombre del producto del carrito es:", VerificarProducto.value(), equalTo(DatosBasicos.nombreProducto)).orComplainWith(MensaExcepciones.class, MensaExcepciones.ProductoNoCorresponde));

    }

    @Entonces("^los datos para la orden de compra son \"([^\"]*)\" y \"([^\"]*)\"$")
    public void losDatosParaLaOrdenDeCompraSonY(String nombreEnOrden, String tarjetaOrden, List<DatosIngreso> datosIngresoList) {

        usuariofinal.attemptsTo(PedirProducto.deProductos());
        usuariofinal.attemptsTo(DiligenciarFormulario.paraOrdenDePago(datosIngresoList.get(0)));
        usuariofinal.attemptsTo(CrearOrden.alComprarProducto());
        usuariofinal.should(seeThat("El nombre en la orden de compra es", VerificarOrden.nombreOrden(), equalTo(nombreEnOrden)).orComplainWith(MensaExcepciones.class, MensaExcepciones.OrdenNoCorresponde));
        usuariofinal.should(seeThat("El numero de la tarjeta de compra es", VerificarOrden.numeroTarjeta(), equalTo(tarjetaOrden)).orComplainWith(MensaExcepciones.class, MensaExcepciones.OrdenNoCorresponde));


    }



}
